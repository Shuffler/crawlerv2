namespace Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FinishDateNullable : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.UrlJobs", "FinishDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.UrlJobs", "FinishDate", c => c.DateTime(nullable: false));
        }
    }
}
