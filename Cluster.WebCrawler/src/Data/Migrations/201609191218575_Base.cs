namespace Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Base : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.UrlJobs", "Host");
        }
        
        public override void Down()
        {
            AddColumn("dbo.UrlJobs", "Host", c => c.String());
        }
    }
}
