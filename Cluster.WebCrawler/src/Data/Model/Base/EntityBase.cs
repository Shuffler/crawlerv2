namespace Data.Model.Base
{
    public abstract class EntityBase : IEntity
    {
        public int Id { get; set; }

    }
}