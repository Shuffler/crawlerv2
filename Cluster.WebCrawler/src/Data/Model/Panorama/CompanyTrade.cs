﻿using Data.Model.Base;

namespace Data.Model.Panorama
{
    public class CompanyTrade : EntityBase
    {
        public CompanyTrade()
        {
        }

        public CompanyTrade(string name, int tradeId)
        {
            Name = name;
            TradeId = tradeId;
        }

        public string Name { get; set; }

        public virtual Trade Trade { get; set; }

        public int TradeId { get; set; }

        public bool Started { get; set; }

        public bool Finished { get; set; }

        public int LastPage { get; set; }
    }
}