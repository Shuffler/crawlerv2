﻿namespace Data.Model.Panorama
{
    public class Phone
    {
        public Phone(string number, PhoneType phoneType, bool primary)
        {
            Number = number;
            PhoneType = phoneType;
            Primary = primary;
        }

        public string Number { get; set; }

        public PhoneType PhoneType { get; set; }

        public bool Primary { get; set; }
    }
}