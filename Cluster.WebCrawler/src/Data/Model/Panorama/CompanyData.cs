﻿using System.Collections.Generic;
using Data.Model.Base;

namespace Data.Model.Panorama
{
    public class CompanyData : EntityBase
    {
        public CompanyData()
        {
            Phones = new List<Phone>();
        }

        public string CompanyName { get; set; }

        public List<Phone> Phones { get; set; }

        public Address Address { get; set; }

    }
}