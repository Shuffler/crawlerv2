﻿using Data.Model.Base;

namespace Data.Model.Panorama
{
    public class Company : EntityBase
    {
        public Company(string link)
        {
            Link = link;
        }

        public Company()
        {
        }

        public virtual CompanyTrade CompanyTrade { get; set; }

        public virtual int CompanyTradeId { get; set; }

        public string Link { get; set; }

        public string Body { get; set; }

        public string Province { get; set; }
    }
}