﻿using Data.Model.Base;

namespace Data.Model.Panorama
{
    public class Trade : EntityBase
    {
        public Trade(string name, string sufix)
        {
            Name = name;
            Sufix = sufix;
        }

        public Trade()
        {
        }

        public string Name { get; set; }
        public string Sufix { get; set; }
    }
}