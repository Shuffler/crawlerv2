﻿using System;
using Data.Model.Base;

namespace Data.Model.General
{
    public class UrlJob : EntityBase
    {
        public string Url { get; set; }
        
        public string Content { get; set; }

        public CrawlJobStatus CrawlStatus { get; set; }
            
        public DateTime RunDate { get; set; }

        public DateTime? FinishDate { get; set; }

    }
}