namespace Data.Model
{
    public enum CrawlJobStatus
    {
        Done = 100,
        New = 1,
        Error = 999
    }
}