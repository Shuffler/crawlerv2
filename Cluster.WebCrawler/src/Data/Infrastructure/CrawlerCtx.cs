﻿using System.Data.Entity;
using Data.Model.General;

namespace Data.Infrastructure
{
    public class CrawlCtx : DbContext
    {
        static CrawlCtx()
        {
            Database.SetInitializer<CrawlCtx>(null);
        }

        public CrawlCtx()
            : base("Name=localDB")
        {
            Database.SetInitializer<CrawlCtx>(null);
        }

        //public DbSet<Trade> Trade { get; set; }
        //public DbSet<CompanyTrade> CompanyTrade { get; set; }
        //public DbSet<Company> Company { get; set; }

        public DbSet<UrlJob> UrlJob { get; set; }
    }
}
