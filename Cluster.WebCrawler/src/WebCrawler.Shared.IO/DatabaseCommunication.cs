﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Akka.Actor;
using Akka.Routing;
using Data.Infrastructure;
using Data.Model;
using Data.Model.General;

namespace WebCrawler.Shared.IO
{
    public class DatabaseCommunication : ReceiveActor
    {

        public DatabaseCommunication()
        {
            //_uriService = uriService;
            Receive<string>(x => ProcessData(x));
            Receive<CrawlUpdate>(x => ProcessCrawlUpdate(x));
        }


        private void ProcessCrawlUpdate(CrawlUpdate crawlUpdate)
        {
            using (var ctx = new CrawlCtx())
            {
                var entity = ctx.UrlJob.SingleOrDefault(x => x.Url == crawlUpdate.Document.DocumentUri.AbsoluteUri);
                if (entity == null)
                {
                    entity = CreateNew(crawlUpdate);
                    ctx.UrlJob.Add(entity);
                    ctx.SaveChanges();
                }
                else
                {
                    UpdateEntity(entity, crawlUpdate);
                    ctx.SaveChanges();
                }
            }
        }

        private void UpdateEntity(UrlJob entity, CrawlUpdate message)
        {
            entity.Url = message.Document.DocumentUri.AbsoluteUri;
            entity.Content = "Test2";
            entity.CrawlStatus = Data.Model.CrawlJobStatus.Done; 
            entity.FinishDate = DateTime.Now;
        }

        private UrlJob CreateNew(CrawlUpdate message)
        {
            var urljob = new UrlJob
            {
                Url = message.Document.DocumentUri.AbsoluteUri,
                Content = "Test",
                CrawlStatus = Data.Model.CrawlJobStatus.New,
                RunDate = DateTime.Now,
            };
            return urljob;
        }

        private static void ProcessData(string str)
        {
            Console.Write(str);
        }


        //private void HandleCrawlStatusUpdateMessage(CrawlUpdate message)
        //{
        //    using (var ctx = new CrawlCtx())
        //    {
        //        var entity = ctx.UrlJob.SingleOrDefault(x => x.Url == message.StatusUpdate.Job.Url.AbsoluteUri);
        //        if (entity == null)
        //        {
        //            entity = CreateNew(message);
        //            ctx.UrlJob.Add(entity);
        //            ctx.SaveChanges();
        //        }
        //        else
        //        {
        //            UpdateEntity(entity, message);
        //            ctx.SaveChanges();
        //        }
        //    }
        //    Sender.Tell(true, Self);
        //}

        //private void UpdateEntity(UrlJob entity, CrawlUpdate message)
        //{
        //    var job = message.StatusUpdate.Job;
        //    entity.Url = job.Url.AbsoluteUri;
        //    entity.Content = message.Content;
        //    entity.CrawlStatus = CrawlStatus.Finished;
        //    entity.FinishDate = DateTime.Now;
        //}

        //private UrlJob CreateNew(CrawlUpdate message)
        //{
        //    var job = message.StatusUpdate.Job;
        //    var urljob = new UrlJob
        //    {
        //        Url = job.Url.AbsoluteUri,
        //        Content = message.Content,
        //        CrawlStatus = CrawlStatus.New,
        //        RunDate = DateTime.Now,
        //    };
        //    return urljob;
        //}

        //private void HandleProcessedJobsMessageMessage(ProcessedJobsMessage message)
        //{
        //    using (var ctx = new CrawlCtx())
        //    {
        //        var uriService = new UriService();
        //        var result = ctx.UrlJob.Where(x => x.CrawlStatus == CrawlStatus.Finished).ToList()
        //                    .Select(x => uriService.CreateUri(x.Url)).ToList();

        //        Sender.Tell(result, Self);
        //    }
        //}

        //private void HandleActiveJobsMessage(ActiveJobsMessage message)
        //{
        //    using (var ctx = new CrawlCtx())
        //    {
        //        var _uriService = new UriService();

        //        var result = ctx.UrlJob.Where(x => x.CrawlStatus != CrawlStatus.Finished).ToList()
        //            .Select(x => new CrawlJob(_uriService.CreateUri(x.Url), null)).ToList();
        //        Sender.Tell(result, Self);
        //    }
        //}
    }
}