namespace WebCrawler.Shared.IO
{
    public enum CrawlJobStatus
    {
        Done = 100,
        Started = 1,
        Error = 999
    }
}