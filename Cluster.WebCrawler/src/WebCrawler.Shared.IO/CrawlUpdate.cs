using System;
using WebCrawler.TrackingService.State;

namespace WebCrawler.Shared.IO
{
    public class CrawlUpdate
    {
        public Uri Uri { get; set; }

        public CrawlDocument Document { get; set; }

        public CrawlUpdate(CrawlDocument document)
        {
            Document = document;
        }
    }
}