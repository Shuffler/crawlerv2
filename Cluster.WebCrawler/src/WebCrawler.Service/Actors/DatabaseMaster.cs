﻿using System;
using System.Linq;
using Akka.Actor;
using Akka.Routing;
using WebCrawler.Messages.Commands;
using WebCrawler.Messages.Commands.V1;
using WebCrawler.Messages.State;
using WebCrawler.TrackingService.Actors.Downloads;
using WebCrawler.TrackingService.Actors.IO;

namespace WebCrawler.TrackingService.Actors
{
    /// <summary>
    /// The very top-level actor. Oversees all requests from front-end machines.
    /// </summary>
    public class DatabaseMaster : ReceiveActor, IWithUnboundedStash
    {
        public const string MasterBroadcastName = "broadcaster";
        protected IActorRef DatabaseBroadcaster;
        protected int OutstandingAcknowledgements;

        public DatabaseMaster()
        {
            Ready();
        }

        protected override void PreStart()
        {
            DatabaseBroadcaster = Context.Child(MasterBroadcastName).Equals(ActorRefs.Nobody)
                ? Context.ActorOf(Props.Empty.WithRouter(FromConfig.Instance), MasterBroadcastName)
                : Context.Child(MasterBroadcastName);
        }

        protected override void PreRestart(Exception reason, object message)
        {
            /* Don't kill the children */
            PostStop();
        }

        public IStash Stash { get; set; }

        private void Ready()
        {
            Receive<string>(start =>
            {
                //DatabaseBroadcaster.Tell(start);
                var members = DatabaseBroadcaster.Ask<Routees>(new GetRoutees()).Result.Members.ToList();
                OutstandingAcknowledgements = members.Count;
                Context.SetReceiveTimeout(TimeSpan.FromSeconds(3.0));
            });
        }
        
       
    }
}
