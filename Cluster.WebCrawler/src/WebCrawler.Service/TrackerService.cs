﻿using System;
using Akka.Actor;
using Akka.Routing;
using Topshelf;
using WebCrawler.Messages.Commands.V1;
using WebCrawler.Messages.State;
using WebCrawler.Shared.IO;
using WebCrawler.TrackingService.Actors;
using WebCrawler.TrackingService.Actors.Downloads;
using WebCrawler.TrackingService.State;

namespace WebCrawler.TrackingService
{
    public class TrackerService : ServiceControl
    {
        protected ActorSystem ClusterSystem;
        protected IActorRef ApiMaster;
        protected IActorRef DownloadMaster;
        public IActorRef DatabaseMaster { get; set; }

        public bool Start(HostControl hostControl)
        {
            ClusterSystem = ActorSystem.Create("webcrawler");
            ApiMaster = ClusterSystem.ActorOf(Props.Create(() => new ApiMaster()), "api");
            DownloadMaster = ClusterSystem.ActorOf(Props.Create(() => new DownloadsMaster()), "downloads");
            DatabaseMaster = ClusterSystem.ActorOf(Props.Create(() => new DatabaseMaster()), "database");
            DatabaseMaster.Tell("test");
            //ApiMaster.Tell(new StartJob(new CrawlJob(new Uri("http://www.rottentomatoes.com/", UriKind.Absolute), true), null));
            return true;
        }


        public bool Stop(HostControl hostControl)
        {
            ClusterSystem.Terminate();
            return true;
        }
    }
}
